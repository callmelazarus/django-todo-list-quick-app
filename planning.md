# One Shot Planning

These are some markdown formatting you can use to plan. You can preview your markdown in VS Code with the command "markdown preview", commonly `Command + Shift + V` (`Ctrl + Shift + V` on Windows) while you have this file open

Edit this and make it your own. Alternatively, link your notion here:

## My Notion / weblink

https://notion...

## Steps

### Step 1

* [ ] First substep
* [ ] Second substep

## Feature 2 - establish project
Create a Django project named brain_two so that the manage.py file is in the top directory.

```
# starting the virtual environment
python -m venv .venv #makes a directory for things to be installed
source .venv/bin/activate   #activates the venv in the current terminal

```


```
# Create a new Django project in the current
# directory, normally the directory that contains
# your .venv directory. DO NOT FORGET THE DOT!
django-admin startproject «name» .
```

Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list

```
# Create a new Django app
python manage.py startapp «name»

# Application definition
# install application, which is found in apps.py

INSTALLED_APPS = [
    "todos.apps.TodosConfig", 
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]
```

Run the migrations
- This will generate a db.sqlite3

```
# Make migrations after creating or changing
# a model class
python manage.py makemigrations

# Apply the migrations to your database after
# making them
python manage.py migrate
```
Create a super user

```
python manage.py createsuperuser #creates admin
```

## Feature 3 - create Todolist Model 
This feature is for you to create a TodoList model in the todos Django app.


Name	Type	Constraints
name	string	maximum length of 100 characters
created_on	date time	should be automatically set to the time the todolist was created

```
# write class Todolist in models.py
class Todolist(models.Model):
  name = models.CharField(max_length=100)
  created_on = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name
```

```
python manage.py makemigrations
python manage.py migrate
```

Testing database:

```
python manage.py shell
```

Once in the shell, you can import the model and use the TodoList.objects.create method to create a new TodoList.
```
>>> from todos.models import TodoList
>>> todolist = TodoList.objects.create(name="Reminders")
```

## Feature 4 - register TodoList model
Register the TodoList model with the admin so that you can see it in the Django admin site.

## Feature 5 - create TodoItem model
This feature is for you to create a TodoItem model in the todos Django app.

Name	Type	Constraints
task	string	maximum length of 100 characters
due_date	date time	should be optional
is_completed	boolean	should default to False
list	foreign key	should be related to the TodoList model and have a related name of "items". It should automatically delete if the to-do list is deleted. (This is a cascade.)


## Feature 6 - Register Todoitem in admin
Register the TodoItem model with the admin so that you can see it in the Django admin site.

## Feature 7 - List of all todolists
You can use either a function view OR a class-based view.

1. Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
- write function based view, remember to query data, and save it in the context, to pass to an appropriate template.

2. Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
- setup file, and appropriate path within app urls.py. 
- Ensure that you are relating to the relevant view function (which needs to be imported)

3. Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
- need to use keyword `include` to reference the todos.urls file
- setup prefix path

4. Create a template for the list view that complies with the following specifications.
- file structure requires the directory `templates`

### Template specifications
The resulting HTML from a request to the path http://localhost:8000/todos/  should result in HTML that has:

the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the content "My Lists"
a table that has two columns:
the first with the header "Name" and the rows with the names of the Todo lists
the second with the header "Number of items" and nothing in those rows because we don't yet have tasks

## Feature 8 - Detail page for todolist
1. Create a view that shows the details of a particular to-do list, including its tasks

2. In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"

3. Create a template to show the details of the todolist and a table of its to-do items

4. Update the list template to show the number of to-do items for a to-do list

5. Update the list template to have a link from the to-do list name to the detail view for that to-do list

### Template for detail view page
the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the to-do list's name as its content
an h2 tag that has the content "Tasks"
a table that contains two columns with the headers "Task" and "Due date" with rows for each task in the to-do list

### Template update for todo list view
Update the todo list view
The to-do list view should now contain an a tag to the to-do list detail page for the indicated to-do list in the table's first column.

The second column should now show the total number of to-do items for a project.


## Feature 9 - create form info for todo list

1. Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList

Form info:
- https://docs.djangoproject.com/en/4.1/topics/forms/modelforms/
- https://docs.djangoproject.com/en/4.1/topics/forms/

2. If the to-do list is successfully created, it should redirect to the detail page for that to-do list

- redirect
https://docs.djangoproject.com/en/4.1/topics/http/shortcuts/

3. Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"

4. Create an HTML template that shows the form to create a new TodoList (see the template specifications below)

```
<form action="/your-name/" method="post">
    {% csrf_token %}
    {{ form }}
    <input type="submit" value="Submit">
</form>
```

5. Add a link to the list view for the TodoList that navigates to the new create view

### Template for Create todolist
the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the content "Create a new list"
a form tag with method "post" that contains any kind of HTML structures but must include:
an input tag with type "text" and name "name"
a button with the content "Create"

### Update the todo list view
You need to add a link to the to-do list view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:

a p tag that contains:
an a tag with an href attribute that points to the create to-do list URL and has content "Create a new list"

## Feature 10 - update todo list detail page
1. Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList

https://docs.djangoproject.com/en/4.1/ref/class-based-views/generic-editing/#updateview

1. If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.

2. Register that view for the path <int:pk>/edit/ in the todos urls.py and the name "todo_list_update".

3. Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).

4. Add a link to the list view for the TodoList that navigates to the new update view.

### The update form
The resulting HTML from a request to the path http://localhost:8000/todos/<int:pk>/edit/ should result in HTML that has:

the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the content "Update list"
a form tag with method "post" that contains any kind of HTML structures but must include:
an input tag with type "text" and name "name"

### update todo detail view

Updating the todo detail view
You need to add a link to the to-do list detail view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:

a p tag that contains:
an a tag with an href attribute that points to the create todo list URL and has content "Edit"

## Feature 11 - delete view functionality
1. Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
`model_instance.delete()` will delete the instance

2. If the to-do list is successfully deleted, it should redirect to the to-do list list view.
`return redirect("todo_list_list")`

3. Register that view for the path <int:pk>/delete/ in the todos urls.py and the name "todo_list_delete".


4. Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).


5. Add a link to the detail view for the TodoList that navigates to the new delete view.


### Delete form
The delete form
The resulting HTML from a request to the path http://localhost:8000/todos/<int:pk>/delete/ should result in HTML that has:

the fundamental five in it
a main tag that contains:
div tag that contains:
a h1 tag that contains the text, "Are you sure?"
a form tag with method "post" that contains any kind of HTML structures but must include:
a button with the content "Delete"

### update todo list detail view with delete

Update the todo detail view
You need to add a link to the to-do list detail view after the h1 tag and before the table. The HTML that you add should be inside the div tag, after the h1 tag, and be:

a p tag that contains:
an a tag with an href attribute that points to the create todo list URL and has content "Delete"

# Feature 12 - Create todo ITEM

1. Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.


2. If the to-do list is successfully created, it should redirect to the detail page for that to-do list.


3. Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".


4. Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).


5. Add a link to the list view for the TodoList that navigates to the new create view.

### The create form
The resulting HTML from a request to the path http://localhost:8000/todos/items/create/  should result in HTML that has:

the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the content "Create a new item"
a form tag with method "post" that contains any kind of HTML structures but must include:
an input tag with type "text" and name "task"
an input tag with type "text" and name "due date"
an input tag with type "checkbox" and name "is_completed"
a select list with the name "list"


# Feature 13 - 

1. Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.

2. If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.

3. Register that view for the path items/<int:pk>/edit/ in the todos urls.py and the name "todo_item_update".

4. Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).

5. Add a link to the list view for the TodoList that navigates to the new update view.


### The update form
The resulting HTML from a request to the path http://localhost:8000/todos/<int:pk>/edit/ should result in HTML that has:

the fundamental five in it
a main tag that contains:
div tag that contains:
an h1 tag with the text "Update item"
a form tag with method "post" that contains any kind of HTML structures but must include:
an input tag with type "text" and name "task"
an input tag with type "text" and name "due date"
an input tag with type "checkbox" and name "is_completed"
a select list with the name "list"
a button with the content "Update"

### The todo list detail view
You need to add links to the to-do list detail view. Each task name in the table should link to the correct to-do list item's edit page url.