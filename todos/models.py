from django.db import models

# create todolist
class Todolist(models.Model):
  name = models.CharField(max_length=100)
  created_on = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name

# create todoItem
# one TodoList can have many totoitems
class Todoitem(models.Model):
  task = models.CharField(max_length=100)
  due_date = models.DateField(null = True, blank = True)
  is_completed = models.BooleanField(default=False)
  list = models.ForeignKey("Todolist", related_name="items", on_delete=models.CASCADE)

  def __str__(self):
    return str(self.task)