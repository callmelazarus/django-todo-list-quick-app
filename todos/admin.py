from django.contrib import admin
from todos.models import Todolist, Todoitem
# Register your models here.


admin.site.register(Todolist)
admin.site.register(Todoitem)