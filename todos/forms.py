from django.forms import ModelForm
from .models import Todolist, Todoitem

# ModelForm will be a helper class to develop Form Classes

# form to create todo List
class ListForm(ModelForm):
  class Meta:
    model = Todolist
    fields = ['name']

# form to create todo Item
class ItemForm(ModelForm):
  class Meta:
    model = Todoitem
    fields = "__all__"