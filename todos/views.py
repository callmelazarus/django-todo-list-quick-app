from django.shortcuts import render, redirect
from todos.models import Todolist, Todoitem
from .forms import ListForm, ItemForm

# Create your views here.


# get all the instances of the todolist model
def show_todolist(request):
  todolists = Todolist.objects.all()
  context = {
    "todolists": todolists
  }
  return render(request, "todos/list.html", context)


# get a detail view of a todolist item, which will include all the items associated with it
def detail_todolist(request, pk):
  todolist = Todolist.objects.get(pk=pk)
  tasks = todolist.items.all() # note that items is the related name
  context = {
    "todolist": todolist,
    "tasks": tasks
  }
  return render(request, "todos/detail.html", context)


# Creating a Todolist - using a form
def create_todolist(request):
  if request.method == 'POST':
    form = ListForm(request.POST)
    if form.is_valid():
      new_form = form.save()
      return redirect("todo_list_detail", pk=new_form.pk)
  
  # if we are dealing with GET request
  else:
    form = ListForm()
  context = {"form": form}
  return render(request, "todos/createlist.html", context) 


# update a todolist name
def update_todolist(request, pk):
  todolist = Todolist.objects.get(pk=pk)
  if request.method == "POST":
    form = ListForm(request.POST, instance=todolist)
    if form.is_valid():
      # if form is acceptable, save it, and redirect to the detail page
      form.save()
      return redirect("todo_list_detail", pk=pk)
  else:
    form = ListForm(instance=todolist)
  
  context = {"form": form}
  return render(request, "todos/updatelist.html", context)

# delete a todolist
def delete_todolist(request, pk):
  todolist = Todolist.objects.get(pk=pk)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")
  # no context necessary. We are simply deleting
  return render(request, "todos/deletelist.html")


# create a todo item
def create_todoitem(request):
  if request.method == 'POST':
    form = ItemForm(request.POST)
    if form.is_valid():
      new_form = form.save()
      # need to ensure that you are looking at list item
      return redirect("todo_list_detail", pk=new_form.list.pk)
  
  # if we are dealing with GET request
  else:
    form = ItemForm()
  context = {"form": form}
  return render(request, "todos/createitem.html", context) 


# update a todo item
def update_todoitem(request, pk):
  todoitem = Todoitem.objects.get(pk=pk)
  if request.method == "POST":
    form = ItemForm(request.POST, instance=todoitem)
    if form.is_valid():
      # if form is acceptable, save it, and redirect to the detail page
      form.save()
      return redirect("todo_list_detail", pk=pk)
  else:
    form = ItemForm(instance=todoitem)
  
  context = {"form": form}
  return render(request, "todos/updateitem.html", context)