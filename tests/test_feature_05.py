from django.test import TestCase
from django.db import models


class TestTodoItemModel(TestCase):
    def test_todoitem_model_exists(self):
        try:
            from todos.models import Todoitem  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models.TodoItem'")

    def test_todoitem_model_has_char_task_field(self):
        try:
            from todos.models import Todoitem

            task = Todoitem.task
            self.assertIsInstance(
                task.field,
                models.CharField,
                msg="Todoitem.task should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.task'")

    def test_todoitem_model_has_task_with_max_length_200_characters(self):
        try:
            from todos.models import Todoitem

            task = Todoitem.task
            self.assertEqual(
                task.field.max_length,
                100,
                msg="The max length of Todoitem.task should be 100",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.TodoItem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.task'")

    def test_todoitem_model_has_task_that_is_not_nullable(self):
        try:
            from todos.models import Todoitem

            task = Todoitem.task
            self.assertFalse(
                task.field.null,
                msg="Todoitem.task should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.TodoItem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.task'")

    def test_todoitem_model_has_task_that_is_not_blank(self):
        try:
            from todos.models import Todoitem

            task = Todoitem.task
            self.assertFalse(
                task.field.blank,
                msg="Todoitem.task should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models.TodoItem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.task'")

    def test_todoitem_model_has_due_date_field(self):
        try:
            from todos.models import Todoitem

            due_date = Todoitem.due_date
            self.assertIsInstance(
                due_date.field,
                models.DateTimeField,
                msg="Todoitem.due_date should be a date time field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'Todoitem.due_date'")

    def test_todoitem_model_has_due_date_that_is_nullable(self):
        try:
            from todos.models import Todoitem

            due_date = Todoitem.due_date
            self.assertTrue(
                due_date.field.null,
                msg="Todoitem.due_date should be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'Todoitem.due_date'")

    def test_todoitem_model_has_due_date_that_is_blank(self):
        try:
            from todos.models import Todoitem

            due_date = Todoitem.due_date
            self.assertTrue(
                due_date.field.blank,
                msg="Todoitem.due_date should be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'Todoitem.due_date'")

    def test_todoitem_model_has_is_completed_field(self):
        try:
            from todos.models import Todoitem

            is_completed = Todoitem.is_completed
            self.assertIsInstance(
                is_completed.field,
                models.BooleanField,
                msg="TodoItem.is_completed should be a boolean field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.TodoItem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.is_completed'")

    def test_todoitem_model_has_is_completed_with_default_value_false(self):
        try:
            from todos.models import Todoitem

            is_completed = Todoitem.is_completed
            self.assertFalse(
                is_completed.field.default,
                msg="Todoitem.due_date should have a default value of False",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.is_completed'")

    def test_todoitem_model_has_list_field(self):
        try:
            from todos.models import Todoitem

            list = Todoitem.list
            self.assertIsInstance(
                list.field,
                models.ForeignKey,
                msg="Todoitem.list should be a ForeignKey field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todoitem'")
        except AttributeError:
            self.fail("Could not find 'Todoitem.list'")

    def test_todoitem_model_has_list_with_related_name_items(self):
        try:
            from todos.models import Todoitem

            list = Todoitem.list
            self.assertEqual(
                list.field._related_name,
                "items",
                msg="Todoitem.list should have a related name of items",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.TodoItem'")
        except AttributeError:
            self.fail("Could not find 'TodoItem.list'")

    def test_todoitem_model_has_foreign_key_with_todolist(self):
        try:
            from todos.models import Todoitem, Todolist

            list = Todoitem.list
            self.assertEqual(
                list.field.related_model,
                Todolist,
                msg="TodoItem.list should have a foreign key with the TodoList model",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail(
                "Could not find 'todos.models.TodoItem' or 'todos.models.TodoList'"
            )
        except AttributeError:
            self.fail("Could not find 'TodoItem.list'")
