from django.test import TestCase
from django.db import models


class TestTodoListModel(TestCase):
    def test_todolist_model_exists(self):
        try:
            from todos.models import Todolist  # noqa: F401
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models.TodoList'")

    def test_todolist_model_has_char_name_field(self):
        try:
            from todos.models import Todolist

            name = Todolist.name
            self.assertIsInstance(
                name.field,
                models.CharField,
                msg="TodoList.name should be a character field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todolist'")
        except AttributeError:
            self.fail("Could not find 'Todolist.name'")

    def test_todolist_model_has_name_with_max_length_200_characters(self):
        try:
            from todos.models import Todolist

            name = Todolist.name
            self.assertEqual(
                name.field.max_length,
                100,
                msg="The max length of Todolist.name should be 100",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.TodoList'")
        except AttributeError:
            self.fail("Could not find 'TodoList.name'")

    def test_todolist_model_has_name_that_is_not_nullable(self):
        try:
            from todos.models import Todolist

            name = Todolist.name
            self.assertFalse(
                name.field.null,
                msg="Todolist.name should not be nullable",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todolist'")
        except AttributeError:
            self.fail("Could not find 'Todolist.name'")

    def test_todolist_model_has_name_that_is_not_blank(self):
        try:
            from todos.models import Todolist

            name = Todolist.name
            self.assertFalse(
                name.field.blank,
                msg="TodoList.name should not be allowed a blank value",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models.TodoList'")
        except AttributeError:
            self.fail("Could not find 'TodoList.name'")

    def test_todolist_model_has_created_on_field(self):
        try:
            from todos.models import Todolist

            created_on = Todolist.created_on
            self.assertIsInstance(
                created_on.field,
                models.DateTimeField,
                msg="Todolist.created_on should be a date time field",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todolist'")
        except AttributeError:
            self.fail("Could not find 'Todolist.created_on'")

    def test_todolist_model_has_created_on_automatically_set(self):
        try:
            from todos.models import Todolist

            created_on = Todolist.created_on
            self.assertTrue(
                created_on.field.auto_now_add,
                msg="Todolist.created_on should automatically set the date",
            )
        except ModuleNotFoundError:
            self.fail("Could not find 'todos.models'")
        except ImportError:
            self.fail("Could not find 'todos.models.Todolist'")
        except AttributeError:
            self.fail("Could not find 'Todolist.created_on'")
